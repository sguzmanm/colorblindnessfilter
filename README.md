# ColorBlindnessFilter

## Computer Vision Lib

- OpenCV 3.4.3: https://sourceforge.net/projects/opencvlibrary/files/opencv-android/3.4.3/
- Tutorial: https://android.jlelse.eu/a-beginners-guide-to-setting-up-opencv-android-library-on-android-studio-19794e220f3c

## App Brief Info
The paper the app is based on has weird results, and yet, even when I tried to make the linear algebra matrix for the transformation, I could not get it to work either. So I decided to present it like this with the paper.
- The idea is to take a photo and it is stored in your gallery.
- There are four filters: - Normal: Your normal image - Protanopia: Red blindness - Duternaopia: Green blindness - Tritanopia: Blue blindness
- The app uses OpenCV for color matrix processing.
- The color matrix operations for each type of color blindness was taken from Lamiaa's study titled "Smartphone Based Image Color Correction
  for Color Blindness" (2018): https://online-journals.org/index.php/i-jim/article/view/8160/5068
- Since sometimes the operations resulted in numbers outside the 8bit scope for each RGB component (range 0-255), there is the option to normalize the result values, that is, to catalogue each value inside this range by getting the absolute value and substracting the number until it reaches the range. This setting is triggered with the checkbox present on the app.
