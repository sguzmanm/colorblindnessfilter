package sguzmanm.uniandes.edu.co.colorblindnessfilter.extensions

fun Double.normalize(): Double {

    var num= Math.abs(this)
    while(num>255)
        num-=255
    return num
}