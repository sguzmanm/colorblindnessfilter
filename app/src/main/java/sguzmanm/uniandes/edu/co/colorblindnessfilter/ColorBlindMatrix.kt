package sguzmanm.uniandes.edu.co.colorblindnessfilter

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import org.opencv.core.CvType
import org.opencv.core.Mat
import sguzmanm.uniandes.edu.co.colorblindnessfilter.extensions.normalize

val lmsConstantcolorMatrix= arrayOf(
    floatArrayOf(17.8824f,43.5161f,4.11935f),
    floatArrayOf(3.45565f,27.1554f,3.86714f),
    floatArrayOf(0.0299566f,0.184309f,1.46709f)
)

val lsmSim= mapOf(
    "Protanopia" to arrayOf(
        floatArrayOf(0f,2.02344f,-2.52581f),
        floatArrayOf(0f,1f,0f),
        floatArrayOf(0f,0f,1f)
    ),
    "Duteranopia" to arrayOf(
        floatArrayOf(1f,0f,0f),
        floatArrayOf(0.49421f,0f,1.24827f),
        floatArrayOf(0f,0f,1f)
    ),
    "Tritanopia" to arrayOf(
        floatArrayOf(1f,0f,0f),
        floatArrayOf(0f,1f,0f),
        floatArrayOf(-0.395913f,0.801109f,0f)
    )
)

val rbgiConstant=arrayOf(
    floatArrayOf(0.0809444479f,-0.130504409f,0.116721066f),
    floatArrayOf(0.113614708f,-0.0102485335f,0.0540193266f),
    floatArrayOf(-0.000365296938f,-0.00412161469f,0.693511405f)
)

val rgbMap= mapOf(
    "Normal" to arrayOf(floatArrayOf()),
    "Protanopia" to arrayOf(
        floatArrayOf(0f,0f,0f),
        floatArrayOf(0.7f,1f,0f),
        floatArrayOf(0.7f,0f,1f)
    ),
    "Duteranopia" to arrayOf(
        floatArrayOf(1f,0.7f,0f),
        floatArrayOf(0f,0f,0f),
        floatArrayOf(0f,0.7f,1f)
    ),
    "Tritanopia" to arrayOf(
        floatArrayOf(1f,0f,0.7f),
        floatArrayOf(0f,1f,0.7f),
        floatArrayOf(0f,0f,0f)
    )
)


fun multiplyMatrices(firstMatrix: Array<DoubleArray>, secondMatrix: Array<DoubleArray>): Array<DoubleArray> {
    val r1=firstMatrix.size
    val c1=firstMatrix[0].size
    val c2=secondMatrix[0].size
    val product = Array(r1) { DoubleArray(c2) }

    for (i in 0 until r1) {
        for (j in 0 until c2) {
            for (k in 0 until c1) {
                product[i][j] += firstMatrix[i][k] * secondMatrix[k][j]
            }
        }
    }
    return product
}

fun sumMatrices(firstMatrix: Array<DoubleArray>, secondMatrix: Array<DoubleArray>,isSum:Boolean): Array<DoubleArray> {

    val r1=firstMatrix.size
    val c1=firstMatrix[0].size
    val factor=if(isSum)1 else -1
    val res = Array(r1) { DoubleArray(c1) }

    for (i in 0 until r1) {
        for (j in 0 until c1) {
                res[i][j] = firstMatrix[i][j] + secondMatrix[i][j]*factor
        }
    }
    return res
}

/*


 */

fun Array<FloatArray>.toDoubleArray():Array<DoubleArray>
{
    val doubleArray= Array(this.size){ doubleArrayOf()}
    for(i in 0 until this.size)
    {
        val ans= this[i].map { it.toDouble() }.toDoubleArray()
        doubleArray[i] = ans
    }
    return doubleArray
}

fun LMSDaltonization(mat: Mat,disease:String,isNormalized:Boolean):Mat{

    if(rgbMap[disease]?.get(0)?.isEmpty()!!)
        return mat
    val newMat=Mat(mat.rows(),mat.height(), CvType.CV_8UC4)
    for(i in 0 until mat.rows())
    {
        for(j in 0 until mat.cols())
        {
            val tempMatrix=mat.get(i,j)

            // RGB SETUP
            val colorMatrix= arrayOf(
                doubleArrayOf(tempMatrix[0]),
                doubleArrayOf(tempMatrix[1]),
                doubleArrayOf(tempMatrix[2])
            )
            val lmsMatrix=multiplyMatrices(lmsConstantcolorMatrix.toDoubleArray(),colorMatrix)
            val simMatrix=multiplyMatrices(lsmSim[disease]!!.toDoubleArray(),lmsMatrix)
            val rgbI=multiplyMatrices(rbgiConstant.toDoubleArray(),simMatrix)
            val dif= sumMatrices(colorMatrix,rgbI,false)
            val shiftColors= multiplyMatrices(rgbMap[disease]!!.toDoubleArray(),dif)
            val ans= sumMatrices(colorMatrix,shiftColors,true)

            var result= listOf(ans[0][0],ans[1][0],ans[2][0])
            if(isNormalized)
            {
                result=result.map { it.normalize() }
            }

            newMat.put(i,j,result[0],result[1],result[2],tempMatrix[3])
        }
    }
    return newMat
}
