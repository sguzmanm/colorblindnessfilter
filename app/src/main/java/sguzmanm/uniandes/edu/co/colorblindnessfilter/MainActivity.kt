package sguzmanm.uniandes.edu.co.colorblindnessfilter

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Mat
import sguzmanm.uniandes.edu.co.colorblindnessfilter.extensions.toBitmap


class MainActivity : AppCompatActivity() {

    // Permission constants
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_IMAGE_SAVE = 2
    //Current data
    var capturedImage = false
    var currentBlindness: String= rgbMap.keys.first()

    lateinit var imageMat:Mat
    var calculatedMat=false
    lateinit var originalBitMap:Bitmap

    // -----------
    // SETUP
    // -----------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private val mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(this){
        override fun onManagerConnected(status:Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS->
                {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    imageMat=Mat()
                }
                else ->
                {
                    super.onManagerConnected(status);
                }
            }
        }
    }

    override fun onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    fun showSnack(msg:String){
            //Snackbar(view)
            val snackbar = Snackbar.make(constraint_layout, msg,
                Snackbar.LENGTH_LONG).setAction(getString(R.string.dismiss), null)
            snackbar.setActionTextColor(Color.BLUE)
            snackbar.show()
    }

    fun setSpinner() {

        val spinner = blindness_spinner
        val spinnerArrayAdapter = ArrayAdapter<String>(
            this, android.R.layout.simple_spinner_item,
            rgbMap.keys.toTypedArray()
        )
        spinnerArrayAdapter.setDropDownViewResource(
            android.R.layout
                .simple_spinner_dropdown_item
        )
        spinner.adapter = spinnerArrayAdapter

        // Set on select action
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (!capturedImage)
                    return;

                apply(parent.getItemAtPosition(position).toString())
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    // ---------------
    // FILTER
    // ---------------
    private fun apply(disease:String){
        if(!calculatedMat)
        {
            val d = imageView.drawable as BitmapDrawable;
            originalBitMap=d.bitmap
            calculatedMat=true
        }

        Utils.bitmapToMat(originalBitMap, imageMat)
        val mat=LMSDaltonization(imageMat,disease,isNormalized.isChecked)
        imageView.setImageBitmap(mat.toBitmap())
        imageMat.release()
        mat.release()
    }

    //----------------
    // IMG HANDLING
    // ----------------
    fun dispatchTakePictureIntent(v: View) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    fun saveCurrentImg(v: View?){
        // Check for permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_IMAGE_SAVE)

            return
        }

        val uri=getImageUri()
        // Parse the gallery image url to uri
        showSnack("Saved at ${uri}")

    }

    private fun getImageUri():Uri{
        // Get the bitmap from drawable object
        val bitmap = loadBitmapFromView(imageView)

        // Save image to gallery
        val savedImageURL = MediaStore.Images.Media.insertImage(
            contentResolver,
            bitmap,
            currentBlindness,
            "Image of $currentBlindness"
        )

        return Uri.parse(savedImageURL)
    }

    private fun loadBitmapFromView(v: View): Bitmap {
        val width = v.width
        val height = v.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val c = Canvas(bitmap)

        v.layout(v.left, v.top, v.right, v.bottom)
        v.draw(c)
        return bitmap
    }

    //--------------
    // RESULTS
    // -------------
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if(data==null || data.extras==null)
                return
            val imageBitmap = data.extras.get("data") as Bitmap
            imageView.setImageBitmap(imageBitmap)
            capturedImage=true
            calculatedMat=false
            setSpinner()
            btn_save_img.text=getString(R.string.save_img)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_IMAGE_SAVE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    saveCurrentImg(null)
                } else {
                    showSnack(getString(R.string.error_save_file))
                }
                return
            }

            else -> {
                // Ignore all other requests.
            }
        }
    }



}
